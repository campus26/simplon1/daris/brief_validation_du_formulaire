
let nom = document.querySelector("#name");
let email = document.querySelector("#email");
let tel = document.querySelector("#phone");
let form = document.querySelector("#signup-form");



//ajouter l'atribut "required" aux 3 inputs
nom.required = true;
email.required = true;
tel.required = true;

// ******** Validation du nom *****************
//On definit le maxlength de l'input name
nom.setAttribute('maxlength',50);

nom.addEventListener("input", onInputName);
nom.addEventListener("focusout", onInputName);

function onInputName(){
    msgname = document.querySelector(".msgname");
    if(nom.value.length < 1){
        nom.style.border = "1px solid red";
        msgname.innerText = "Le nom n'est pas valide.";
        return false;
    }else{
        nom.style.border = "1px solid green";
        msgname.innerText = "";
        return true;
    }

}

//juste bordure de l'input name en vert sur focus
//nom.addEventListener("focus", () => nom.style.border = "1px solid green"); 


//******    Validation du num de tel        ******
tel.addEventListener("input", onInputTel);
tel.addEventListener("focusout", onInputTel);

function onInputTel(){             
    
    str = tel.value;
    console.log(str);
    msg = document.querySelector(".msg");
    c1 = str[0];
    c2 = str[1];
    
    if(str.length < 10 || str.length > 10 || c1 != 0 || (c2 != 6 && c2 != 7)){
        tel.style.border = "1px solid red";
        msg.innerText = "Le numéro n'est pas valide.";
        return false;
    }else{
        tel.style.border = "1px solid green";
        msg.innerText = "";
        return true;
    }

}

//******    Validation de l'email       *****
email.addEventListener("input", onInputEmail); 
email.addEventListener("focusout", onInputEmail);

function onInputEmail(e){
    let emailvalue = email.value;
    
    console.log(emailvalue);
    msgemail = document.querySelector(".msgemail");

    //let regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    let regex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    if(emailvalue.match(regex)){
        email.style.border = "1px solid green";
        msgemail.innerText = "";
        return true;
    }else{
        email.style.border = "1px solid red";
        msgemail.innerText = "L'email n'est pas valide.";
        return false;
        
    }
}


//****** Validation du formulaire avec evenement submit ********
form.addEventListener("submit", onFormSubmit);

function onFormSubmit(e){
    e.preventDefault();
    //console.log(e);
    console.log(onInputName());
    console.log(onInputTel());
    console.log(onInputEmail());
    if( onInputName() && onInputTel() && onInputEmail() ){
        console.log('test');
        window.alert("Les entrées sont correectes");
    }else{
        window.alert("Les entrées ne sont pas correectes");
    }

}
